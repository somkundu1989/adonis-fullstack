'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Academicyear extends Model {
  static get table() {
    return 'academic_years'
  }
}

module.exports = Academicyear
