'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Student extends Model {
  static get table() {
    return 'students'
  }
  user()
  {
    return this.belongsTo('App/Models/User', 'user_id')
  }
  childparent()
  {
    return this.hasOne('App/Models/Childparent', 'childparent_id', 'id')
  }
  studentGrades()
  {
    return this.hasMany('App/Models/Studentgrade', 'id', 'student_id')
      //.innerJoin('grades', 'student_grade.grade_id', '=', 'grades.id')
      .innerJoin('academic_years', 'student_grade.acedemic_year_id', '=', 'academic_years.id')
      .where('academic_years.is_activate', 1)
      //.select('student_grade.grade_id', 'grades.title as grade', 'academic_years.title as academicyear')
      //.first()
  }
}

module.exports = Student
