'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Virtualclasss extends Model {
  static get table() {
    return 'virtual_classes'
  }
}

module.exports = Virtualclasss
