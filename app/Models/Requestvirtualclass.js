'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Requestvirtualclass extends Model {
  static get table() {
    return 'request_virtual_classes'
  }
}

module.exports = Requestvirtualclass
