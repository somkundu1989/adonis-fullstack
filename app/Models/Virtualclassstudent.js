'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Virtualclassstudent extends Model {
  static get table() {
    return 'virtual_class_student'
  }
}

module.exports = Virtualclassstudent
