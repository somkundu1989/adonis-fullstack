'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Assignmentstudent extends Model {
  static get table() {
    return 'assignment_student'
  }
}

module.exports = Assignmentstudent
