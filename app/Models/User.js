'use strict'

/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use('Hash')

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class User extends Model {
  static boot () {
    super.boot()

    /**
     * A hook to hash the user password before saving
     * it to the database.
     */
    this.addHook('beforeSave', async (userInstance) => {
      if (userInstance.dirty.password) {
        userInstance.password = await Hash.make(userInstance.password)
      }
    })
  }

  /**
   * A relationship on tokens is required for auth to
   * work. Since features like `refreshTokens` or
   * `rememberToken` will be saved inside the
   * tokens table.
   *
   * @method tokens
   *
   * @return {Object}
   */
  tokens () {
    return this.hasMany('App/Models/Token', 'user_id')
  }
  student() {
    return this.hasOne('App/Models/Student', 'user_id');
  }
  childparent()
  {
    return this.hasOne('App/Models/Childparent', 'user_id');
  }
  teacher()
  {
    return this.hasOne('App/Models/Teacher', 'user_id');
  }
  roleUsers() {
    return this.hasMany('App/Models/RoleUser', 'user_id');
  }
}

module.exports = User
