'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Requestvirtualclassstudent extends Model {
  static get table() {
    return 'request_virtual_class_student'
  }
}

module.exports = Requestvirtualclassstudent
