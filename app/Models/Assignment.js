'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Assignment extends Model {
  static get table() {
    return 'assignments'
  }
}

module.exports = Assignment
