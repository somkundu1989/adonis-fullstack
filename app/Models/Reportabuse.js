'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Reportabuse extends Model {
  static get table() {
    return 'report_abuses'
  }
}

module.exports = Reportabuse
