'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Permissionrole extends Model {
  static get table() {
    return 'permission_role'
  }
}

module.exports = Permissionrole
