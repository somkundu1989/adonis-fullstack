'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Studentgrade extends Model {
  static get table() {
    return 'student_grade'
  }
  student()
  {
    return this.belongsTo('App/Models/Student', 'student_id', 'id');
  }
  acedemicyear()
  {
    return this.belongsTo('App/Models/Academicyear', 'acedemic_year_id', 'id');
  }
  grade()
  {
    return this.belongsTo('App/Models/Grade', 'grade_id', 'id');
  }
}

module.exports = Studentgrade
