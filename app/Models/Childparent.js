'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Childparent extends Model {
  static get table() {
    return 'childparents'
  }

  user()
  {
    return this.belongsTo('App/Models/User','user_id');
  }
  ownchildren()
  {
    return this.hasMany('App/Models/Student', 'childparent_id');
  }
}

module.exports = Childparent
