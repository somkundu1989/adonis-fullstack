'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Teacher extends Model {
  static get table() {
    return 'teachers'
  }
  user()
  {
      return this.belongsTo('App/Models/User', 'user_id');
  }
}

module.exports = Teacher
