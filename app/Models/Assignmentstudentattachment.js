'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Assignmentstudentattachment extends Model {
  static get table() {
    return 'assignment_student_attachment'
  }
}

module.exports = Assignmentstudentattachment
