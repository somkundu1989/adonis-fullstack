'use strict'
const User = use('App/Models/User')
const Student = use('App/Models/Student')
const Teacher = use('App/Models/Teacher')
const Role = use('App/Models/Role')
const Env = use('Env')

class UserController {
    async index({request, response, view}){
      let params = request.all()
      let page = (params.page) ? params.page : 1
      let perPage = (params.perPage) ? params.perPage : Env.get('PERPAGE')
      let roleType = (params.roleType) ? params.roleType : 'students'
      let list = []
      if (roleType == 'students') {
        list = await Student.query()
          .innerJoin('users', 'students.user_id', '=', 'users.id')
          .where(function () {
            if(params.searchText){
              this
                .where('users.email', 'like', '%'+params.searchText+'%')
                .orWhere('students.first_name', 'like', '%'+params.searchText+'%')
                .orWhere('students.middle_name', 'like', '%'+params.searchText+'%')
                .orWhere('students.last_name', 'like', '%'+params.searchText+'%')
            }else{
              this
            }
          })
          .with('user')
          .with('childparent')
          .with('studentGrades.grade')
          //.fetch()
          .paginate(page, perPage)
      }
      let roles = await Role.query().select('display_name', 'name', 'id').fetch()
      //console.log(roles.toJSON()/*list.toJSON(), Env.get('PERPAGE'), request.all().page*/ )
      return view.render('users.index', { result: list.toJSON(), roleType: roleType, roles: roles.toJSON(), searchText: params.searchText })
    }
}

module.exports = UserController
